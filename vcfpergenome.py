#!/usr/bin/python3.4
import traceback

import argparse
from collections import defaultdict

from Bio import SeqIO
from Bio.Seq import Seq

from seqseqpan.io import Parser
from seqseqpan.exception import ConsensusFastaIdxFormatError, CoordinateOutOfBoundsError
from seqseqpan.mapper import Mapper


def parseVCF(vcf_f, path, name):
    coords_dict = {"single": [], "multi": []}
    ref_dict = {}

    with open(vcf_f, "r") as f, open(path + "/" + name + "_ALTlonger.vcf", "w") as output:
        line = f.readline()

        while line:
            if not line == "":
                if line.startswith("#"):
                    output.write(line)
                else:
                    fields = line.split("\t")
                    pos = int(fields[1])  # positions .vcf file is 1-based - as is xmfa and mapper works with 1-based positions!
                    ref = fields[3]
                    alt = fields[4]

                    ref_len = len(ref)

                    if len(alt) > 1:
                        output.write(line)
                    else:
                        ref_dict[pos] = {"ref": ref, "alt": alt}
                        if ref_len > 1:
                            coords_dict["multi"].append(list(range(pos, pos + ref_len)))
                        else:
                            coords_dict["single"].append(pos)
            line = f.readline()

    return coords_dict, ref_dict


def printMappedFiles(coords_dict, ref_dict, genomes, alignment, path, name):
    # output positions are one-based as in .vcf file
    header = "POS\tREF\tALT\tPOS_GENOME\tREF_GENOME\n"

    genome_dict = {}
    for genome_nr in genomes:
        genome_dict[genome_nr] = open(path + "/" + name + "_genome-" + genome_nr + "_mapped.txt", "w")
        genome_dict[genome_nr].write("#" + alignment.genomes[int(genome_nr)].file_path + "\n")
        genome_dict[genome_nr].write(header)

    try:

        for coord in sorted(coords_dict):
            for genome_nr in genomes:
                genome_entry = coords_dict[coord].get(genome_nr, {"pos": "-", "seq": "-"})
                coord_l = genome_entry["pos"]
                seq = genome_entry["seq"]

                if type(coord_l) == list and len(coord_l) == 0:
                    coord_l = "-"
                    seq = "-"

                pos_genome = str(coord_l) if not type(coord_l) is list else str(coord_l[0]) if len(
                    coord_l) == 1 else "-".join([str(min(coord_l)), str(max(coord_l))])
                genome_dict[genome_nr].write(
                    '\t'.join([str(coord), ref_dict[coord]["ref"], ref_dict[coord]["alt"], pos_genome, seq]) + "\n")
    except Exception:
        traceback.print_exc()
        print(genome_entry)

    for genome_nr, handle in genome_dict.items():
        handle.close()


def main():
    global args

    parser = Parser()
    mapper = Mapper()

    ### parse vcf and get coordinates
    coords_source_dict, ref_dict = parseVCF(args.vcf_f, args.output_p, args.output_name)

    try:
        sparse_align, sparse_consensus = parser.parse_consensus_index(args.consensus_f)
    except ConsensusFastaIdxFormatError as e:
        print(e.message)
    else:

        # do mapping in groups:  single position variant: no groups necessary, 
        #                        multiple positions: combine maps from A to B (add list to coord_dict)

        try:

            coords_dict = mapper.map_coordinates(sparse_align, sparse_consensus, "c", args.genomes,
                                                 coords_source_dict["single"] + sum(coords_source_dict["multi"], []))
            for coord_l in coords_source_dict["multi"]:
                c_dict = {c: coords_dict.pop(c) for c in coord_l}

                min_c = min(coord_l)

                mapped_dict = defaultdict(list)
                for coord in coord_l:
                    mapped = c_dict[coord]

                    for genome in args.genomes:
                        mapped_dict[genome].append(mapped.get(genome, "-"))

                coords_dict[min_c] = mapped_dict
        except CoordinateOutOfBoundsError as e:
            print (e.message)
        else:
            genome_dict = {}
            for genome_nr in args.genomes:
                genome_dict[genome_nr] = SeqIO.read(open(sparse_align.genomes[int(genome_nr)].file_path), "fasta")

            for coord, info in coords_dict.items():
                for genome_nr, g_coords in info.items():
                    to_reverse_complement = False
                    if type(g_coords) == list:

                        # Bio.Seq works with 0-based positions!

                        sequence = ''.join((lambda g_dict=genome_dict[genome_nr], g_coords=g_coords: [
                            (g_dict.seq[abs(i) - 1] if i != "-" else "-") for i in g_coords])())
                        g_coords = [a for a in g_coords if a != "-"]
                        to_reverse_complement = (len(g_coords) > 0 and g_coords[0] < 0)
                    else:
                        sequence = genome_dict[genome_nr].seq[abs(g_coords) - 1] if g_coords != "-" else "-"
                        to_reverse_complement = (g_coords < 0)

                    if to_reverse_complement:
                        sequence = Seq(sequence)
                        sequence = str(sequence.reverse_complement())

                    coords_dict[coord][genome_nr] = {"seq": sequence, "pos": g_coords}

            printMappedFiles(coords_dict, ref_dict, args.genomes, sparse_align, args.output_p, args.output_name)

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-p", "--output_path", dest="output_p", help="path to output directory", required=True)
    parser.add_argument("-n", "--name", dest="output_name",
                        help="file prefix for output file", required=True)
    parser.add_argument("-c", "--consensus", dest="consensus_f", help="consensus FASTA file used in XMFA",
                        required=True)
    parser.add_argument("-v", "--vcf", dest="vcf_f", help="VCF file based on consensus to be mapped to genomes",
                        required=True)
    parser.add_argument("-g", "--genomes", dest="genomes", help="ids of genomes (order as in xmfa/consensus idx file)",
                        nargs="+", required=True, type=str)

    args = parser.parse_args()

    main()
