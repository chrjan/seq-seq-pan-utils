import argparse
import operator
import re
import pdb

from seqseqpan.io import Parser

def comp_ne(c1, c2):
    if c1 == "-" or c2 == "-":
        return False
    else:
        return operator.ne(c1,c2)

def hamming(str1, str2):
    assert len(str1) == len(str2)
    if args.ignore_gaps:
        return sum(map(comp_ne, str1, str2))
    else:
        return sum(map(operator.ne, str1, str2))

def main():
    global args

    parser = Parser()
    alignment = parser.parse_xmfa(args.xmfa_f)

    diff = 0

    for lcb in alignment.lcbs:

        entry_a = lcb.get_entry(args.genome1)
        entry_b = lcb.get_entry(args.genome2)

        if entry_a is not None and entry_b is not None:

            diff += hamming(entry_a.sequence, entry_b.sequence)
        elif entry_a is not None and not args.ignore_gaps:
            diff += len(re.sub("-", "", entry_a.sequence))  # count all except '-' (missing sequence in entry counts as gap)
        elif entry_b is not None and not args.ignore_gaps:
            diff += len(re.sub("-", "", entry_b.sequence))

    print(diff)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-x", "--xmfa", dest="xmfa_f", help="XMFA input file", required=True)
    parser.add_argument("-a", dest="genome1", help="Number of first genome (as listed in genomedescription", required=True, type=int)
    parser.add_argument("-b", dest="genome2", help="Number of second genome (as listed in genomedescription", required=True, type=int)
#    parser.add_argument("-p", "--output_path", dest="output_p", help="path to output directory", required=True)
#    parser.add_argument("-n", "--name", dest="output_name", help="file prefix for output FASTA file", required=True)
    parser.add_argument("-g", "--ignoregaps", dest="ignore_gaps", help="do not count gaps as difference", action='store_true')

    args = parser.parse_args()

    main()
