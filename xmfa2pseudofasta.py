import argparse
import re
from collections import defaultdict

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

from seqseqpan.io import Parser

def dict_to_fasta(seq_dict, file_name, fasta_headers):
    records = []

    for genome, seq_list in seq_dict.items():
        sequence = ''.join(seq_list)
        if sequence.strip('-'): # if sequence contains characters other than '-'
            gid = str(genome)
            gid = fasta_headers.get(gid, gid)
            records.append(SeqRecord(Seq(sequence), gid, description=''))

    with open(file_name + ".fasta", "w") as handle:
        SeqIO.write(records, handle, "fasta")


def main():
    global args

    parser = Parser()

    alignment = parser.parse_xmfa(args.xmfa_f)

    sorted_lcbs = alignment.get_sorted_lcbs(args.order)

    genomes = set(alignment.genomes.keys())
    num_genomes = len(genomes)
    seq_dict_list = []
    seq_dict = defaultdict(list)
    seq_dict_length = 0
    for lcb in sorted_lcbs:
        if args.all and len(lcb.entries) < num_genomes:
            continue

        if lcb.length < args.lcbsize:
            continue
            
        cur_genomes = set()

        # start new dict if current one too long. if max_len == 0 use only one dict
        # if seq_dict is empty, current lcb is longer than max_len --> separate dict for this lcb (output sequence will be longer than max len)
        if args.maximum_len > 0 and seq_dict_length > 0 and ((seq_dict_length + lcb.length) > args.maximum_len):
            seq_dict_list.append(seq_dict)
            seq_dict = defaultdict(list)
            seq_dict_length = 0

        for entry in lcb.entries:
            seq = entry.sequence
            if args.replace:
                seq = re.sub(r'[^acgtACGTNX-]', 'N', seq)

            seq_dict[entry.genome_nr].append(seq)
            cur_genomes.add(entry.genome_nr)

        for genome in (genomes - cur_genomes):
            seq_dict[genome].append("-"*lcb.length)

        seq_dict_length+=lcb.length

    seq_dict_list.append(seq_dict)

    # headers for FASTA file
    fasta_headers = dict()
    if args.fasta_header_f is not None:
        with open(args.fasta_header_f, "r") as header_in:
            for line in header_in:
                fields = line.strip().split("\t")
                fasta_headers[fields[0]] = fields[1]


    if len(seq_dict_list) == 1:
        dict_to_fasta(seq_dict_list[0], args.output_p + "/" + args.output_name, fasta_headers)
    else:
        for i in range(0,len(seq_dict_list)):
            dict_to_fasta(seq_dict_list[i], args.output_p + "/" + args.output_name + "_" + str(i+1), fasta_headers)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-x", "--xmfa", dest="xmfa_f", help="XMFA input file", required=True)
    parser.add_argument("-p", "--output_path", dest="output_p", help="path to output directory", required=True)
    parser.add_argument("-n", "--name", dest="output_name",
                        help="file prefix for output FASTA file", required=True)
    parser.add_argument("-o", "--order", dest="order", type=int, default=0,
                        help="ordering of blocks in output [default: %(default)s]", required=False)
    parser.add_argument("-r", "--replace", dest="replace", help="Flag for replacing extended IUPAC characters with N", action='store_true')
    parser.add_argument("-a", "--all", dest="all", help="Use only LCBs with ALL genomes for FASTA sequences.", action='store_true')
    parser.add_argument("-m", "--maximum_len", dest="maximum_len", help="Max length for sequences per output file. If '0', all sequences in one file. Default:0", default=0, type=int)
    parser.add_argument("--fasta_headers", dest="fasta_header_f", help="File with headers used for FASTA file. Format:  genome_nr \t header", default=None)
    parser.add_argument("-f", "--filterlcbsize", dest="lcbsize", help="Minimum size of LCBs included in output", type=int, default=0)
    args = parser.parse_args()

    main()
