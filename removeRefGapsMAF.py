import argparse

from seqseqpan.io import Parser, Writer
from seqseqpan.base import LCB
from seqseqpan.resolver import Resolver

def main():
    parser = Parser()
    writer = Writer()
    resolver = Resolver()
    alignment = parser.parse_xmfa(args.xmfa_f)

    new_lcbs = []
    for lcb in alignment.lcbs:
        ref_entry = lcb.get_entry(args.order)
        if ref_entry is not None:

            splits = sorted(list(ref_entry.gaps.keys()) + list(ref_entry.gaps.values()))
            if len(splits) > 0:
                if splits[0] > 0:
                    splits = [0] + splits

                if splits[-1] != lcb.length:
                    splits.append(lcb.length)

                lcbs = [LCB() for _ in range(len(splits) - 1)]
                for s_idx in range(1, len(splits)):
                    for entry in lcb.entries:

                        new_entry = resolver.get_split_entry(entry, splits[s_idx - 1], splits[s_idx])
                        if new_entry is not None:
                            lcbs[s_idx - 1].add_entries(new_entry)

                new_lcbs.extend(lcbs)
            else:
                new_lcbs.append(lcb)
        else:
            new_lcbs.append(lcb)

    alignment.lcbs = new_lcbs
    writer.write_xmfa(alignment, args.output_p, args.output_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Remove parts of alignments with gaps in reference genome. Output alignment is not valid due to missing data from all other genomes!")
    parser.add_argument("-x", "--xmfa", dest="xmfa_f", help="XMFA input file", required=True)
    parser.add_argument("-p", "--output_path", dest="output_p", help="path to output directory", required=True)
    parser.add_argument("-n", "--name", dest="output_name",
                        help="file prefix for output FASTA file", required=True)
    parser.add_argument("-o", "--order", dest="order", type=int, default=0, help="Id of reference genome", required=False)

    args = parser.parse_args()

    main()
