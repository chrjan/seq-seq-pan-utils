# Utils for working with seq-seq-pan

### Prerequisites
* Python3.4
* seqseqpan module (https://gitlab.com/chrjan/seq-seq-pan/tree/master/seqseqpan)
    * has to be in $PYTHON_PATH or same directory as scripts

**Please note that I wrote these scripts mainly for myself and that they are NOT tested properly. Use at your own risk!**


### Summary
| name        | description | additional prerequisites |
|-------------|-------------|---------------|
|change_strand.py | Reverse complement all blocks in input XMFA.| |
|check_sequence.py | Check whether all blocks show correct sequence compared to original FASTA files.| Biopython: Seq, SeqIO|
|create_xmfa_formauve.py | Reformat XMFA so it can be visualized with mauve viewer. ||
|extract_alignment_part.py | Extract whole alignment parts between start and end positions.||
|removeRefGapsMAF.py | Remove parts of alignments with gaps in reference genome. ||
|vcfpergenome.py | Map variants in VCF relative to consensus sequence to all genomes in pan-genome.| Biopython: Seq, SeqIO |
|xmfa2pseudofasta.py | Concatenate alignment blocks to create Multi-FASTA file.| Biopython: Seq, SeqIO, SeqRecord |

# Other utils
|name| description | prerequisites|
|-------------|-------------|---------------|
|sort_genomelist.Rscript | Sort genomes by similarity and dissimilarity using D2 z-scores.| R, d2z* |

\* Download at: http://veda.cs.uiuc.edu/cgi-bin/d2z/download.pl

