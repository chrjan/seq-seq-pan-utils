import argparse

from seqseqpan.io import Parser, Writer
from seqseqpan.base import SequenceEntry

def main():
    global args

    parser = Parser()
    writer = Writer()

    alignment = parser.parse_xmfa(args.xmfa_f)

    firstLCB = alignment.lcbs[0]
    lcbgenomes = {e.genome_nr for e in firstLCB.entries}
    genomes = set(alignment.genomes.keys())

    for genome in (genomes - lcbgenomes):
        e = SequenceEntry(genome, 0, 0, "+", "-" * firstLCB.length)
        firstLCB.entries.append(e)

    alignment.lcbs[0] = firstLCB

    writer.write_xmfa(alignment, args.output_p, args.output_name, 0, check_invalid=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser("This will change the first LCB to make it viewable with Mauve.")
    parser.add_argument("-x", "--xmfa", dest="xmfa_f", help="XMFA input file", required=True)
    parser.add_argument("-p", "--output_path", dest="output_p", help="path to output directory", required=True)
    parser.add_argument("-n", "--name", dest="output_name",
                        help="file prefix for output file", required=True)
    args = parser.parse_args()

    main()