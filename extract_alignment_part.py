import argparse

from seqseqpan.io import Parser

def main():
    global args

    parser = Parser()

    alignment = parser.parse_xmfa(args.xmfa_f)

    filename_prefix = args.output_p + "/" + args.output_name

    mauve_block_header = '> {0}:{1}-{2} {3} {4} \n'

    for idx in range(len(args.starts)):
        start = int(args.starts[idx])
        end = int(args.ends[idx])
        with open("_".join([filename_prefix, str(start), str(end), "alignmentpart.txt"]), "w") as outf:

            seq_len_prev = 0
            first=True
            for lcb in alignment.lcbs:

                seq_len_cur = seq_len_prev + lcb.length

                if seq_len_cur < start:
                    seq_len_prev = seq_len_cur
                    continue

                entry_start = start - seq_len_prev - 1 if first else 0

                entry_end = min(lcb.length, end - seq_len_prev)

                for entry in lcb.entries:

                    offset_start = (entry_start - entry.sequence[0:entry_start].count("-"))
                    offset_end = (entry_end - entry.sequence[0:entry_end].count("-"))
                    if entry.strand == "+":
                        start_pos = entry.start + offset_start
                        end_pos = entry.start + offset_end - 1
                    else:
                        start_pos = entry.end - offset_end
                        end_pos = entry.end - offset_start

                    outf.write(mauve_block_header.format(entry.genome_nr,
                                                                 start_pos,
                                                                 end_pos,
                                                                 entry.strand,
                                                                 ""
                                                                 )
                                 )
                    outf.write(entry.sequence[entry_start:entry_end])
                    outf.write("\n")
                outf.write("= LCB nr. {0}\n".format(lcb.number))
                seq_len_prev = seq_len_cur

                first = False
                if seq_len_cur > end:
                    break

if __name__ == '__main__':
    parser = argparse.ArgumentParser("This will extract parts of an XMFA file. There will be one file for each set of start and end coordinates.")
    parser.add_argument("-x", "--xmfa", dest="xmfa_f", help="XMFA input file", required=True)
    parser.add_argument("-s", "--starts", dest="starts", nargs='+', help="start coordinate on consensus sequence.", required=True)
    parser.add_argument("-e", "--ends", dest="ends", nargs='+', help="end coordinate on consensus sequence.", required=True)
    parser.add_argument("-p", "--output_path", dest="output_p", help="path to output directory", required=True)
    parser.add_argument("-n", "--name", dest="output_name",
                        help="file prefix for output file", required=True)
    args = parser.parse_args()

    if len(args.starts) != len(args.ends):
        parser.error("Lists of start coordinates and end coordinates must be of equal length!")

    main()
